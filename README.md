# LearningIssuesTracker

This is just a sample project to explore using GitLab’s Issues system.

## Contributing

There’s no contributing to this project, because it’s just a throwaway example.

## Legal

Copyright ©2024 Grant Neufeld.
